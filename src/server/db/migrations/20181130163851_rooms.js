
exports.up = function(knex, Promise) {
    return knex.schema.createTable('rooms', (table) => {
        table.increments();
        table.integer('bid_owner').nullable();
        table.string('users').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('rooms');
};
