
exports.up = function(knex, Promise) {
    return knex.schema.createTable('articles', (table) => {
        table.increments();
        table.integer('bid_user').nullable();
        table.integer('bid_blog').nullable();

        table.string('title').notNullable();
        table.string('content').notNullable();
        table.string('image_url').notNullable();

        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('articles');
};
