const Router = require('koa-router');

const router = new Router();

const send = (ctx, data) => {
  ctx.websocket.send(JSON.stringify(data));
};

router.get('/ping', async ctx => {
  if (ctx.isAuthenticated()) {
    ctx.websocket.on('message', data => {
      const message = (() => {
        try {
          return JSON.parse(data);
        } catch (err) {
          return {};
        }
      })();

      console.log(message);
      switch (message.type) {
        case 'PING':
          return send(ctx, { type: 'PING', payload: { message: 'PONG' } });
        case 'CHAT_NEW_MESSAGE':
          return send(ctx, { type: 'SOCKET_SEND_SUCCESS', payload: message });
        default:
          return send(ctx, {
            type: 'SOCKET_SEND_ERROR',
            payload: { type: 'UNKNOWN_MESSAGE_TYPE' },
          });
      }
    });

    ctx.websocket.on('close', (code, reason) => {
      console.log(`Closing WebSocket: [${code}] ${reason}`);
    });
  } else {
    ctx.websocket.close(1000, 'User not logged in');
  }
});

module.exports = router;
