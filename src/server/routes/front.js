const fs = require('fs');
const mime = require('mime');

module.exports = async ctx => {
  const match = ctx.url.match(/^\/(?!api)(.*)/);

  if (match) {
    const resource = match[1];
    const uri = `./front/build/${resource}`;

    if (fs.existsSync(uri) && fs.lstatSync(uri).isFile()) {
      ctx.type = mime.getType(uri);
      ctx.body = fs.createReadStream(uri);
    } else {
      ctx.type = 'text/html';
      ctx.body = fs.createReadStream('./front/build/index.html');
      ctx.replacedUrl = '/index.html';
    }
  }
};
