const Router = require('koa-router');
const passport = require('koa-passport');
const fs = require('fs');
const queries = require('../db/queries/users');

const { objectWithoutKey } = require('../utils.js');

const router = new Router();

router.post('/auth/register', async ctx => {
  let user = null;

  try {
    user = await queries.addUser(ctx.request.body);
  } catch (err) {
    console.error(err);
  }

  return passport.authenticate('local', (err, user, info, status) => {
    if (user) {
      ctx.login(user);

      const safeUser = objectWithoutKey(user, 'password');

      ctx.type = 'application/json';
      ctx.status = 200;
      ctx.body = {
        user: safeUser,
        status: 'success',
        code: 200,
      };
    } else {
      ctx.type = 'application/json';
      ctx.status = 400;
      ctx.body = {
        message: 'Username already taken',
        status: 'error',
        code: 400,
      };
    }
  })(ctx);
});

router.get('/auth/status', async ctx => {
  if (ctx.isAuthenticated()) {
    const safeUser = objectWithoutKey(ctx.req.user, 'password');

    ctx.type = 'application/json';
    ctx.status = 200;
    ctx.body = {
      user: safeUser,
      status: 'success',
      code: 200,
    };
  } else {
    ctx.type = 'application/json';
    ctx.status = 401;
    ctx.body = {
      message: 'User not logged in',
      status: 'error',
      code: 401,
    };
  }
});

router.post('/auth/login', async ctx => {
  return passport.authenticate('local', (err, user, info, status) => {
    if (user) {
      ctx.login(user);

      const safeUser = objectWithoutKey(user, 'password');

      ctx.type = 'application/json';
      ctx.status = 200;
      ctx.body = {
        user: safeUser,
        status: 'success',
        code: 200,
      };
    } else {
      ctx.type = 'application/json';
      ctx.status = 400;
      ctx.body = {
        message: 'Invalid credentials',
        status: 'error',
        code: 400,
      };
    }
  })(ctx);
});

router.post('/auth/logout', async ctx => {
  if (ctx.isAuthenticated()) {
    ctx.logout();

    ctx.type = 'application/json';
    ctx.status = 200;
    ctx.body = {
      message: 'Logged out',
      status: 'success',
      code: 200,
    };
  } else {
    ctx.type = 'application/json';
    ctx.status = 401;
    ctx.body = {
      message: 'Already logged out',
      status: 'error',
      code: 401,
    };
  }
});

module.exports = router;
