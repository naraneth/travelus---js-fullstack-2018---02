const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const session = require('koa-session');
const passport = require('koa-passport');
const websockify = require('koa-websocket');

const chalk = require('chalk');

//const userRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');
const socketRoutes = require('./routes/socket');
const frontHandler = require('./routes/front');

const app = websockify(new Koa());
const PORT = process.env.PORT || 1337;

const vc = verb =>
  ({
    HEAD: 'cyan',
    CONNECT: 'cyan',
    OPTIONS: 'cyan',
    TRACE: 'cyan',
    GET: 'green',
    POST: 'blue',
    PUT: 'yellow',
    PATCH: 'yellow',
    DELETE: 'red',
  }[verb] || 'bold');

// Logger
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.get('X-Response-Time');
  const replacedUrl = ctx.replacedUrl;
  const url = replacedUrl
    ? `${ctx.url} ${chalk.bold.magenta('->')} ${replacedUrl}`
    : ctx.url;

  console.log(`[${rt}] ${chalk[vc(ctx.method)](ctx.method)} ${url}`);
});

// X-Response-Time
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

// sessions
app.keys = [
  '\xA5O\xB7?!\xA4\xF8\x16m>$\xD0\xAD\xB3\x8E\xE4\xCEc\xDCa\xCE\x9Fo\x03',
];
app.use(session(app));

// Body parser
app.use(bodyParser());

// Authentication
require('./auth');
app.use(passport.initialize());
app.use(passport.session());
app.ws.use(passport.initialize());
app.ws.use(passport.session());

// Routes
app.use(authRoutes.routes());
app.ws.use(socketRoutes.routes()).use(socketRoutes.allowedMethods());

// Mount React front
app.use(frontHandler);

// Server
const server = app.listen(PORT, () => {
  console.log(`Server listening on port: ${PORT}`);
});

module.exports = server;
