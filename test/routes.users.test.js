/*
process.env.NODE_ENV = 'test';

const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const server = require('../src/server/index');
const knex = require('../src/server/db/connection');


describe('GET /api/_/users', () => {
  it('should return all users', (done) => {
    chai.request(server)
    .get('/api/_/users')
    .end((err, res) => {
      // there should be no errors
      should.not.exist(err);
      // there should be a 200 status code
      res.status.should.equal(200);
      // the response should be JSON
      res.type.should.equal('application/json');
      // the JSON response body should have a
      // key-value pair of {"status": "success"}
      res.body.status.should.eql('success');
      // the JSON response body should have a
      // key-value pair of {"data": [3 user objects]}
      //res.body.data.length.should.eql();
      // the first object in the data array should
      // have the right keys
      res.body.data[0].should.include.keys(
        'id', 'firstname', 'lastname','username','email','avatar'
      );
      done();
    });
  });
});

describe('GET /api/_/users/:id', () => {
  it('should respond with a single user', (done) => {
    chai.request(server)
    .get('/api/_/users/3')
    .end((err, res) => {
      // there should be no errors
      should.not.exist(err);
      // there should be a 200 status code
      res.status.should.equal(200);
      // the response should be JSON
      res.type.should.equal('application/json');
      // the JSON response body should have a
      // key-value pair of {"status": "success"}
      res.body.status.should.eql('success');
      // the JSON response body should have a
      // key-value pair of {"data": 1 user object}
      res.body.data[0].should.include.keys(
        'id', 'firstname', 'lastname','username','email','avatar'
      );
      done();
    });
  });
  it('should throw an error if the user does not exist', (done) => {
    chai.request(server)
    .get('/api/_/users/9999999')
    .end((err, res) => {
      // there should an error
      should.exist(err);
      // there should be a 404 status code
      res.status.should.equal(404);
      // the response should be JSON
      res.type.should.equal('application/json');
      // the JSON response body should have a
      // key-value pair of {"status": "error"}
      res.body.status.should.eql('error');
      // the JSON response body should have a
      // key-value pair of {"message": "That user does not exist."}
      res.body.message.should.eql('That user does not exist.');
      done();
    });
  });
});

describe('POST /api/_/users', () => {
  it('should return the user that was added', (done) => {
    chai.request(server)
    .post('/api/_/users')
    .send({
      firstname: 'FN5',
      lastname: 'LN5',
      username: 'testUserPost5',
      email: 'FN5.LN5@gmail.com',
      encrypted_password:'test'
    })
    .end((err, res) => {
      // there should be no errors
      should.not.exist(err);
      // there should be a 201 status code
      // (indicating that something was "created")
      res.status.should.equal(201);
      // the response should be JSON
      res.type.should.equal('application/json');
      // the JSON response body should have a
      // key-value pair of {"status": "success"}
      res.body.status.should.eql('success');
      // the JSON response body should have a
      // key-value pair of {"data": 1 user object}
      res.body.data[0].should.include.keys(
        'id'
      );
      done();
    });
  });
  
  it('should throw an error if the payload is malformed', (done) => {
    chai.request(server)
    .post('/api/_/users')
    .send({
      username: 'testUserPost',
    })
    .end((err, res) => {
      // there should an error
      should.exist(err);
      // there should be a 400 status code
      res.status.should.equal(400);
      // the response should be JSON
      res.type.should.equal('application/json');
      // the JSON response body should have a
      // key-value pair of {"status": "error"}
      res.body.status.should.eql('error');
      // the JSON response body should have a message key
      should.exist(res.body.message);
      done();
    });
  });
});

describe('PUT /api/_/users', () => {
  it('should return the user that was updated', (done) => {
    knex('users')
    .select('*')
    .then((user) => {
      const userObject = user[0];
      chai.request(server)
      .put(`/api/_/users/${userObject.id}`)
      .send({
        username: 'admin18'
      })
      .end((err, res) => {
        // there should be no errors
        should.not.exist(err);
        // there should be a 200 status code
        res.status.should.equal(200);
        // the response should be JSON
        res.type.should.equal('application/json');
        // the JSON response body should have a
        // key-value pair of {"status": "success"}
        res.body.status.should.eql('success');
        // the JSON response body should have a
        // key-value pair of {"data": 1 user object}
        res.body.data[0].should.include.keys(
          'id', 'firstname', 'lastname','username','email','avatar'
        );
        // ensure the user was in fact updated
        const newUserObject = res.body.data[0];
        newUserObject.username.should.not.eql(userObject.username);
        done();
      });
    });
  });
  it('should throw an error if the user does not exist', (done) => {
    chai.request(server)
    .put('/api/_/users/99999')
    .send({
      username: 'admin2'
    })
    .end((err, res) => {
      // there should an error
      should.exist(err);
      // there should be a 404 status code
      res.status.should.equal(404);
      // the response should be JSON
      res.type.should.equal('application/json');
      // the JSON response body should have a
      // key-value pair of {"status": "error"}
      res.body.status.should.eql('error');
      // the JSON response body should have a
      // key-value pair of {"message": "That user does not exist."}
      res.body.message.should.eql('That user does not exist.');
      done();
    });
  });
});

describe('DELETE /api/_/users/:id', () => {
  it('should return the user that was deleted', (done) => {
    knex('users')
    .select('*')
    .then((users) => {
      const userObject = users[3];
      const lengthBeforeDelete = users.length;
      chai.request(server)
      .delete(`/api/_/users/${userObject.id}`)
      .end((err, res) => {
        // there should be no errors
        should.not.exist(err);
        // there should be a 200 status code
        res.status.should.equal(200);
        // the response should be JSON
        res.type.should.equal('application/json');
        // the JSON response body should have a
        // key-value pair of {"status": "success"}
        res.body.status.should.eql('success');
        // the JSON response body should have a
        // key-value pair of {"data": 1 user object}
        res.body.data[0].should.include.keys(
          'id', 'firstname', 'lastname','username','email','avatar'
        );
        // ensure the user was in fact deleted
        knex('users').select('*')
        .then((updatedUsers) => {
          updatedUsers.length.should.eql(lengthBeforeDelete - 1);
          done();
        });
      });
    });
  });
  it('should throw an error if the user does not exist', (done) => {
    chai.request(server)
    .delete('/api/_/users/9999999')
    .end((err, res) => {
      // there should an error
      should.exist(err);
      // there should be a 404 status code
      res.status.should.equal(404);
      // the response should be JSON
      res.type.should.equal('application/json');
      // the JSON response body should have a
      // key-value pair of {"status": "error"}
      res.body.status.should.eql('error');
      // the JSON response body should have a
      // key-value pair of {"message": "That user does not exist."}
      res.body.message.should.eql('That user does not exist.');
      done();
    });
  });
  it('should throw an error if the payload is malformed', (done) => {
    chai.request(server)
    .post('/api/_/users')
    .send({
      username: 'Titanic'
    })
    .end((err, res) => {
      // there should an error
      should.exist(err);
      // there should be a 400 status code
      res.status.should.equal(400);
      // the response should be JSON
      res.type.should.equal('application/json');
      // the JSON response body should have a
      // key-value pair of {"status": "error"}
      res.body.status.should.eql('error');
      // the JSON response body should have a message key
      should.exist(res.body.message);
      done();
    });
  });
});
*/